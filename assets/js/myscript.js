// @authors Charles Longines Longino | Arianne Nicole Batac

$(document).ready(function() {
    $('#table_list').DataTable({
    });

    $('#user-id').change( function () {
    	var user_id = $(this).val();
    	$.ajax({
    		type: 'POST',
    		data: {
    			id : user_id
    		},
    		url: 'getApplicant',
    		dataType: 'json',
    		success: function(data)
    		{
    			$('#user-fname').val(data.firstname);
    			$('#user-mname').val(data.middlename);
    			$('#user-lname').val(data.lastname);
    			$('#user-address').val(data.address);
    			$('#user-birthdate').val(data.birthday);
    			$('#user-birthplace').val(data.birthplace);
    		}
    	});
    });
});
