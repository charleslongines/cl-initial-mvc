### CL INITIAL MVC ###

This MVC setup will serve as a starting point of almost every project. This setup is modified to improve the initial MVC setup used in CodeIgniter.

Changes/Added Feature

A. Added in / application/core/ folder:
A.1 Added MY_Controller.php - this will serve as the Base Controller of this setup.
A.2 Added MY_Loader.php - this will serve as a loader of template/view/model or anything that can be loaded repetitively or in group.
A.3 Added Public_Controller.php - this will save as the Base Controller extended to MY_Controller for the features/menu/page/etc. that is publicly visible, no login/session 		handling needed.
A.4 Added User_Controller - this will serve as the Base Controller extended to MY_Controller for the features that has login/session/account handling, for the mean time, 		this also has the validation Admin account/permission/access.

B. Added/Modified in /application/config/ folder
B.1 Added line of codes after the last line in config.php file, this was added for the configuration of Base(s) Controller


C Modified the Core for this Initial Setuo
C.1 Added assets folder, global_model.php and modified MY_Controller
C.2 Added base controllers: MY_Controller, Public_Controller, User_Controller
Added templates view
C.3 Fix for importing css and javascripts
C.4 Fix/Added initial javascripts and css
C.5 Improved MY_Controller, Public_Controller, User_Controller