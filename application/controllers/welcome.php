<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends Public_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function initialize()
	{
		// REQUIRED TO CALL EVERY CONTROLLER
		// to modify css and javascripts in every controller
		$css = array(
				base_url().'assets/dataTables/dataTables.bootstrap.min.css rel="stylesheet" ',
				base_url().'assets/bower_components/datatables-responsive/css/responsive.dataTables.scss" '
				);
		$this->setCss($css);

		$scripts = array(
				base_url().'assets/bower_components/datatables/media/js/jquery.dataTables.min.js',
				base_url().'assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js'
				);
		$this->setScripts($scripts);
		//
	}

	public function index()
	{	
		$this->loadTemplate(array('welcome_message' => NULL));
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */