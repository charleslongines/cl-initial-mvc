<!-- * @authors Charles Longines Longino | Arianne Nicole Batac -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="Charles Longines Longino | Arianne Nicole Batac">
	<meta name="description" content="">
	
	<title><?=$title;?></title>

	<?php
		// Load CSS and JavaScript from the Controller
		foreach($css as $key => $value)
		{
			// the first(") found in this line is the opening double quote
			// while the second (") or the closing double quote is attached in the array from the controller
			echo '<link href='.$value.' />';
		}

		foreach($scripts as $key => $value)
		{
			echo '<script src='.$value.'></script>';
		}
		//
	?>

</head>