<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

function get_data_table($results, $actions = array())
{
	$CI =& get_instance();
	$controller = $CI->uri->segment(1);
	$url = base_url().$controller;

	$header = array();
	$headerKey = $results[0];

	if(array_key_exists('id', $headerKey))
	{
		unset($headerKey['id']);
	}

	// TABLE PART
	$table_header = array_keys($headerKey);

	$table_modal = '<table id="table_list" class="table table-bordered table-striped dt-responsive" width="100%">';
	$table_modal.=	'<thead>';
	$table_modal.=		'<tr>';
	$table_modal.=			'<th>No.</th>';
					foreach($table_header as $key => $value)
					{
						$table_modal.= '<th>'.$value.'</th>';
					}
					if($actions)
					{
						$table_modal.= '<th>Actions</th>';
					}
	$table_modal.=		'</tr>';
	$table_modal.= 	'</thead>';
	$table_modal.=	'<tbody>';

	if($results)
	{
		foreach($results as $key => $value)
		{
			$table_modal.= '<tr>';
				$table_modal.= '<td>'.++$key.'.</td>'; // Record counter
			foreach($headerKey as $hkey => $hvalue)
			{
				$table_modal.= '<td>'.$value[$hkey].'</td>';
			}
			if($actions)
			{
				foreach($actions as $akey => $avalue)
				{
					switch ($avalue) {
						case 'edit':
							$link = '<button class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal_action" data-title="Edit" data-url="'.$url.'/edit/'.$value['id'].'" data-btn_type="btn-info" title="Edit" style="margin:2px;"><span class="glyphicon glyphicon-edit"></span></button>';
							break;

						case 'delete':
							$link.= '<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal_action" data-title="Delete" data-url="'.$url.'/delete/'.$value['id'].'" data-btn_type="btn-danger" title="Delete" style="margin:2px;"><span class="glyphicon glyphicon-trash"></span></button>';
							break;
						
						default:
							# code...
							break;
					}
				}
				$table_modal.= '<td>'.$link.'</td>';
			}
			$table_modal.= '</tr>';
		}
	}

	$table_modal.=	'</tbody>';
	$table_modal.= '</table>';
	//

	// MODAL PART
	$table_modal.= '<div id="modal_action" class="modal fade" tabindex="-1" role="dialog">';
	$table_modal.=		'<div class="modal-dialog">';
	$table_modal.= 			'<div class="modal-content">';
	$table_modal.=				'<div class="modal-header">';
	$table_modal.= 					'<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
	$table_modal.=						'<span aria-hidden="true">&times;</span>';
	$table_modal.=					'</button>';
	$table_modal.=					'<h4 class="modal-title"></h4>';
	$table_modal.=				'</div>';
	$table_modal.=				'<div class="modal-body">';
	$table_modal.=					"<p>Are you sure you want to proceed?</p>";
	$table_modal.=				'</div>';
	$table_modal.=				'<div class="modal-footer">';
	$table_modal.=					'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
	$table_modal.=					'<a href="" class="btn" style="margin:2px;" id="link"></a>';
	$table_modal.=				'</div>';
	$table_modal.=			'</div>';
	$table_modal.=		'</div>';
	$table_modal.= '</div>';
	//

	return $table_modal;
}

?>