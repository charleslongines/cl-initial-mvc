<?php
/**
 * @authors Charles Longines Longino | Arianne Nicole Batac
 */

class Global_model extends CI_Model
{
	public function getAllRecords($table, $select = '*', $order_by = array())
	{
		$this->db->select($select);
		
		if(!empty($order_by))
		{
			$this->db->order_by($order_by['filter'], $order_by['sort']);
		}

		$query = $this->db->get($table);

		return $this->returnQueryArray($query);
	}

	public function getRecordById($table, $id, $select = '*')
	{
		$this->db->select($select);
		$this->db->where('id', $id);
		$query = $this->db->get($table);

		return $this->returnQueryArray($query);
	}

	public function getRecordByField($table, $data, $select = '*')
	{
		$this->db->select($select);
		$this->db->where($data);
		$query = $this->db->get($table);

		return $this->returnQueryArray($query);
	}

	public function returnQueryArray($query)
	{
		if($query->num_rows())
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function insert($table, $data)
	{
		$this->db->set($data);
		$query = $this->db->insert($table);
		return $query = $this->db->insert_id();
	}

	public function deleteById($table, $id)
	{
		$this->db->where('id',$id);
		return $query = $this->db->delete($table);
	}

	public function deleteByField($table, $data)
	{
		$this->db->where($data);
		return $query = $this->db->delete($table);
	}
	
	public function countAllRecords($table)
	{
		return $query = $this->db->count_all($table);
	}

	public function countRecordByField($table, $data)
	{
		$this->db->select('*');
		$this->db->where($data);

		return $query = $this->db->count_all_results($table);
	}

	public function updateById($table, $id, $data = array())
	{
		if(!empty($data) && !empty($id))
		{
			$this->db->where('id', $id);
			$this->db->update($table, $data);
			return $this->db->affected_rows();
		}
		else
		{
			return FALSE;
		}
	}

	public function updateByField($table, $where = array(), $data = array())
	{
		if(!empty($data) && !empty($where))
		{
			$this->db->where($where);
			$this->db->update($table, $data);
			return $this->db->affected_rows();
		}
		else
		{
			return FALSE;
		}
	}
}
?>