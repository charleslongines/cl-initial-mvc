<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* /application/core/User_Controller.php
*
* @authors Charles Longines Longino
*/

class User_Controller extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->check_session();
		$this->user_menu = $this->getUserMenu();
	}

	public function check_session()
	{
		if(!$this->session->userdata('user_session'))
		{
			$this->session->set_flashdata('msg','Please Login');
			redirect('', 'refresh');
		}
		else
		{
			$user_session = $this->session->userdata('user_session');

			$this->user_id = $user_session['user_id'];
			$this->user_name = $user_session['name'];
			$this->access_level = $user_session['access_level'];
		}
	}

	public function getUserMenu()
	{
		$result = array();
		// Sample data, assume there's a table
		$select = array('menu_id', 'menu_code', 'menu_label');
		$result = $this->global->getAllRecords('menu_table', $select);

		if($result)
		{
			// Assume, has sub-menu for each menu
			foreach($result as $key => $value)
			{
				$result[$key]['sub_menu'] = $this->getPublicSubMenu($value['menu_id']);
			}

			$menu = $result;
		}
		else
		{
			// Create Menu using $key => $value pair where:
			// $key = url link
			// $value = Display menu
			$menu = array(
					array('menu_code' => 'sample-menu-url',
						'menu_label' => 'Sample Menu')
					);
		}

		return $menu;
	}

	public function getUserSubMenu($menu_id)
	{
		$select = array('sub_menu_code', 'sub_menu_label');
		$where = array('menu_id' => $menu_id);
		$result = $this->global->getRecordByField('sub_menu_table', $where, $select);

		return $result;
	}
}

?>