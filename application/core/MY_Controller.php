<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* /application/core/MY_Controller.php
*
* @authors Charles Longines Longino | Arianne Nicole Batac
*/

class MY_Controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Manila");
		
		$this->load->model('global_model','global');
		$this->getGlobalVariable();
		$this->initialize();
	}

	public function getGlobalVariable()
	{
		$this->today = date("Y-m-d H:i:s");
		$this->title = "Title"; // Change For Title
		$this->header = '';
		$this->sub_header = '';

		// ALERT MESSAGES
		$this->msg_success = '<div class="alert alert-success">Record Successfully Created</div>';
		$this->msg_update = '<div class="alert alert-success">Record Successfully Updated</div>';
		$this->msg_deleted = '<div class="alert alert-warning">Record Successfully Deleted</div>';
		$this->msg_error = '<div class="alert alert-danger">Something went wrong, request cant be processed for a moment.</div>';
		//
	}

	// Modify in child controller
	public function initialize()
	{

	}

	public function loadTemplate($templates = array(), $return = FALSE)
	{
		$data['title'] = $this->title;
		$data['scripts'] = $this->getScripts();
		$data['css'] = $this->getCss();

		$content = $this->load->view('templates/header_view', $data);

		// USE THIS IF THE PROJECT NEEDS TO HAVE SIDEBAR
		// if($this->session->userdata('user_session'))
		// {
		// 	$content.= $this->view('templates/sidebar_view', $data);
		// }
		//

		// templates is an array, whereas,
		// key  = name of view
		// value = data passed to view
		foreach($templates as $key => $value)
		{
			$content.= $this->load->view($key, $value, $return);
		}
		
		$content.= $this->load->view('templates/footer_view', NULL);

		return $content;
	}

	public function loadModels($models)
	{
		// models is an array, whereas,
		// key  = name of model
		// value = will serve as an alias of the model
		$content = "";
		
		foreach($models as $key => $value)
		{
			$content.= $this->load->model($key, $value);
		}
		return $content;
	}

	public function setScripts($added_scripts = '')
	{		
		$default_scripts = array(
						base_url().'assets/bower_components/jquery/dist/jquery.min.js',
						base_url().'assets/bower_components/bootstrap/dist/js/bootstrap.min.js',
						base_url().'assets/bower_components/metisMenu/dist/metisMenu.min.js',
						base_url().'assets/dist/js/sb-admin-2.js',
						base_url().'assets/js/myscript.js'
						);

		if($added_scripts)
		{
			$this->scripts = array_merge($default_scripts, $added_scripts);
		}
		else
		{
			$this->scripts = $default_scripts;
		}

		return $this;
	}


	public function getScripts()
	{
		return $this->scripts;
	}

	public function setCss($added_css = '')
	{
		$default_css = array(
						base_url().'assets/bower_components/bootstrap/dist/css/bootstrap.min.css rel="stylesheet"',
						base_url().'assets/bower_components/metisMenu/dist/metisMenu.min.css rel="stylesheet"',
						base_url().'assets/dist/css/sb-admin-2.css rel="stylesheet"',
						base_url().'assets/bower_components/font-awesome/css/font-awesome.min.css rel="stylesheet"',
						);

		if($added_css)
		{
			$this->css = array_merge($default_css, $added_css);
		}
		else
		{
			$this->css = $default_css;
		}

		return $this;
	}

	public function getCss()
	{
		return $this->css;
	}
}
?>