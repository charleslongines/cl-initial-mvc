<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* /application/core/Public_Controller.php
*
* @authors Charles Longines Longino
*/

class Public_Controller extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->public_menu = $this->getPublicMenu();
	}

	public function getPublicMenu()
	{
		$result = array();
		// Sample data, assume there's a table for dynamic menu
		// $select = array('menu_id','menu_code','menu_label');
		// $result = $this->global->getAllRecords('menu_table', $select);

		if($result)
		{
			// Assume, has sub-menu for each menu
			foreach($result as $key => $value)
			{
				$result[$key]['sub_menu'] = $this->getPublicSubMenu($value['menu_id']);
			}

			$menu = $result;
		}
		else
		{	
			// Create Menu using $key => $value pair where:
			// $key = url link
			// $value = Display menu
			$menu = array(
					array('menu_code' => 'sample-menu-url',
						'menu_label' => 'Sample Menu')
					);
		}

		return $menu;
	}

	public function getPublicSubMenu($menu_id)
	{
		//Sample data, assume sub-menu table exist for dynamic menu
		$select = array('sub_menu_code', 'sub_menu_label');
		$where = array('menu_id' => $menu_id);
		$result = $this->global->getRecordByField('sub_menu_table', $where, $select);

		return $result;
	}
}

?>